FROM node:latest
COPY app /usr/src/
WORKDIR /usr/src/
EXPOSE 3000
CMD ["node", "/usr/src/app.js"]
