const http = require('http');
const fs = require('fs');

const hostname = '0.0.0.0';
const port = 3000;

const allow = ['/index.html', '/ver.html'];

let handleRequest = (req, res) => {
	res.writeHead(200, {
		'Content-Type': 'text/html'
	});

	url = (req.url === '/') ? '/index.html' : req.url;

	let file = allow.includes(url) 
		? '.' + url 
		: null;


	if (file) {
		fs.readFile(file, null, function (err, data) {
			if (err) {
				res.writeHead(404);
				res.end('Error: File not found!');
			} else {
				res.end(data);
			}
		});
	} else {
		res.writeHead(403);
		res.end('Error: Forbidden!');
	}
};

const server = http.createServer(handleRequest);

server.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}/`);
});
